public class Server {
    private String Status;
    private String Group;
    private String Version;
    private String Artifact;

    public String getStatus() {
        return Status;
    }

    public String getGroup() {
        return Group;
    }

    public String getVersion() {
        return Version;
    }

    public String getArtifact() {
        return Artifact;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public void setGroup(String group) {
        Group = group;
    }

    public void setVersion(String version) {
        Version = version;
    }

    public void setArtifact(String artifact) {
        Artifact = artifact;
    }
}
