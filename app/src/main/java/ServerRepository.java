import android.util.Log;

import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServerRepository {
    private IServerWService wService;
    public ServerRepository() {
        Retrofit retro = new Retrofit.Builder()
                .baseUrl("https://hillcroftinsurance.com:8445")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        this.wService=retro.create(IServerWService.class);

    }
    public void getStatus(){
        this.wService.getStatus().enqueue(new Callback<Server>() {
            @Override
            public void onResponse(Call<Server> call, Response<Server> response) {
                Server s = response.body();
                Log.e("SRES",s.getStatus());

            }

            @Override
            public void onFailure(Call<Server> call, Throwable t) {

            }
        });
    }
}